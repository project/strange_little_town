<?php
?><div class="comment <?php print comment_classes($comment) .' '. $zebra .' '. $status ?> clear-block">

  <?php if ($title): ?>
    <h3 class="title"><?php print $title ?>
      <?php if ($comment->new): ?><span class="new"><?php print $new ?></span><?php endif; ?>
    </h3>
  <?php endif; ?>

  <div class="comment-info clear-block">

    <?php if ($submitted): ?>
        <span class="submitted">
          <?php print str_replace('(not verified)', '(visitor)', $author); ?>
          <span class="post-date"><?php print (format_date($comment->timestamp, 'custom', 'F jS, Y g:i A O')); ?></span>
        </span>

    <?php endif; ?>

  </div>

  <div class="content">
    <?php if ($picture): ?><?php print $picture ?><?php endif; ?>

    <?php print $content ?>
    <?php if ($signature): ?>
    <div class="user-signature clear-block">
      <?php print $signature ?>
    </div>
    <?php endif; ?>
  </div>

  <?php if ($links): ?>
    <?php print $links ?>
  <?php endif; ?>  
</div>